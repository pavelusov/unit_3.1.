var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0],
    students = [],
    points = [],
    max, maxIndex;

// 1 Наполянем массивы
// studentsAndPoints.forEach(function (value, i) {
//     if (i % 2 == 0){
//         students.push(value);
//     }else {
//         points.push(value);
//     }
// });
students = studentsAndPoints.filter(function (value, i) {
    return !(i % 2);
});
points = studentsAndPoints.filter(function (value, i) {
    return i % 2;
});
// 2 Выводим список студентов
console.log("Список студентов:");
students.forEach(function (value, i) {
    console.log("Студент " + value + " набрал " + points[i] + " баллов" );
});

// 3 Найти студента
console.log();
console.log("3 пункт");
// points.forEach(function (value, i) {
//     if (!max || max < value){
//         max = value;
//         maxIndex = i;
//     }
// });
max = points.reduce(function (preview, current, i) {
        if (preview < current){
            maxIndex = i;
            // console.log("Если preview меньше чем current итерация %d значение %d. Preview = %d", i, current, preview);
            // console.log("****" + preview + "<" + current);
            // console.log("max = %d", current);
            return current;
        }else {
            // console.log("Иначе preview больше чем current итерация %d значение %d. Preview = %d", i, current, preview);
            // console.log("****" + preview + ">" + current);
            // console.log("max = %d", preview);
            return preview;
        }

}, 0);
// console.log("Студент набравший максимальный балл: " + students[maxIndex] + " (" + max + " баллов)");
console.log("Студент набравший максимальный балл: " + students[maxIndex] + " (" + max + " баллов)");
// 4 Увеличить баллы
console.log();
console.log("4 пункт");
points = points.map(function (value, i) {
    if (students[i] == "Ирина Овчинникова" || students[i] == "Александр Малов"){
        return value + 30; // Убрал +=
    }else {
        return value;
    }


});

console.log("Список студентов:");
students.forEach(function (value, i) {
    console.log("Студент " + value + " набрал " + points[i] + " баллов" );
});